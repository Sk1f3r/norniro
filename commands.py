SDWAN_CONFIGS = ["show running-config", "show sdwan running-config"]

ONLY_CONFIG = ["show running-config"]

SHOW_TECH = ["show tech-support"]

CISCO_SHARED_COMMANDS = [
    "show version", "show inventory", "show running-config"
]

IOS_COMMANDS = [
    "show interfaces status", "show interfaces description",
    "show interfaces switchport", "show mac address-table", "show ip arp",
    "show ip interface brief", "show interfaces trunk", "show vlan",
    "show cdp neighbors", "show lldp neighbors",
    "show ip dhcp snooping binding", "show logging", "show spanning-tree",
    "show switch detail", "show env all", "show etherchannel summary",
    "show interfaces transceiver", "show vtp status", "show ip route",
    "show ntp status", "show ntp associations", "show processes cpu sorted",
    "show processes memory sorted"
] + CISCO_SHARED_COMMANDS

NXOS_COMMANDS = [
    "show interface status", "show interface description",
    "show interface switchport", "show mac address-table", "show ip arp",
    "show ip interface brief", "show interface trunk", "show vlan",
    "show cdp neighbors", "show lldp neighbors", "show spanning-tree",
    "show environment", "show port-channel summary",
    "show interface transceiver", "show ip route", "show ntp peer-status",
    "show vrrp", "show logging", "show vpc", "show route-map",
    "show processes cpu sort", "show processes memory sort"
] + CISCO_SHARED_COMMANDS

ASA_COMMANDS = [
    "show interface ip brief", "show vlan", "show arp", "show cpu usage",
    "show failover", "show failover state", "show conn", "show xlate",
    "show nameif", "show perfmon"
] + CISCO_SHARED_COMMANDS
