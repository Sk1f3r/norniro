# IMPORT
from os import path, mkdir
from datetime import datetime
from time import strftime
from logging import basicConfig, getLogger

from commands import *

from nornir import InitNornir
from nornir.plugins.tasks import networking
from nornir.core.filter import F

# LOG
FORMAT = '[%(asctime)s] | %(message)s'
basicConfig(format=FORMAT, level="WARNING")
log = getLogger(__name__)

# VAR
ROOT_DIR = path.dirname(__file__)
DATA_DIR = path.join(ROOT_DIR, "data")
NR_CONFIG = "inventory/config.yaml"
FAILED = []


# FUNCTION
def task_send_command(task, command):
    """ To send a command via Netmiko and write down output """

    # performing a task run
    result = task.run(networking.netmiko_send_command,
                      command_string=command,
                      strip_prompt=True,
                      strip_command=True,
                      enable=True)

    # if got something
    if result:
        # prepare vars
        host = task.host.name  # router1
        site = task.host.get('site')  # site1
        cmd = command.replace(" ", "-")  # show-version
        file_name = f"{host}_{cmd}.log"  # router1_show-version.log
        timemark = strftime(r'%Y-%m-%d')  # 2018-01-23
        dirname = f'{site}_{timemark}'  # site1_2018-01-23

        # create root directory and subdirectories if required
        prepare_dir(dirname)

        # a name of a file to put result into
        # %somepath%/data/site1_2018-01-23/router1_show-version.log
        full_file_name = path.join(DATA_DIR, dirname, file_name)

        # preparing vars for header of meta information
        timestamp = datetime.now().__str__()
        header_p1 = f"~ hostname: {host}\n~ command: {command}\n"
        header_p2 = f"~ site: {site}\n~ timestamp: {timestamp}\n\n"

        # write down a header and a result
        with open(full_file_name, "w") as f:
            header = header_p1 + header_p2
            f.write(header)
            f.write(result[0].result)

        return True
    return None


def run_send_command(nr, _command, **kwargs):
    """ To filter and run a task """
    # filter by name
    if 'name' in kwargs:
        nr = nr.filter(name=kwargs.get('name'))
    # filter by role
    if 'role' in kwargs:
        nr = nr.filter(role=kwargs.get('role'))
    # filter by hostname
    if 'host' in kwargs:
        nr = nr.filter(hostname=kwargs.get('host'))
    # filter by type
    if 'type' in kwargs:
        nr = nr.filter(type=kwargs.get('type'))
    # filter by group
    if 'group' in kwargs:
        nr = nr.filter(F(groups__contains=kwargs.get('group')))
    # filter by tags
    if 'tags' in kwargs:
        nr = nr.filter(F(tags__contains=kwargs.get('tags')))
        # nr = nr.filter(F(tags__all=kwargs.get('tags')))
    # deny to run without filters
    if not kwargs:
        answer = input("Do ya rly wanna run for all? [y/n] ")
        if answer.strip() != "y":
            raise ValueError("Denying to run for all devices")

    # if some hosts left after filtering then run commands
    if nr.inventory.hosts:
        print(f'Running command "{_command}" for following hosts:')
        for host in nr.inventory.hosts.keys():
            print(host, sep=',')
        result = nr.run(task=task_send_command, command=_command)
        # ['failed_hosts']
    else:
        print(f"!!! List of hosts is empty")
        return None

    if result.failed:
        for host in result.failed_hosts:
            FAILED.append(host)
        return False

    return True


def prepare_dir(site):
    """ To create a root dir (data) and subdirectories to store outputs """

    # removing all symbols not allowed by OS from a name of a site
    forbidden_chars = r'\/:*?"<>|'
    _site = [char for char in site if char not in forbidden_chars]
    site = "".join(_site)

    # create the parent data dir if not exists
    # %somepath%/data/
    try:
        if not path.isdir(DATA_DIR):
            mkdir(DATA_DIR)
    except Exception as e:
        raise

    # create a dir for an every site if not exists
    # %somepath%/data/site1_2018-01-23/
    site = path.join(DATA_DIR, site)
    try:
        if not path.isdir(site):
            mkdir(site)
    except Exception as e:
        raise


def main():
    """ Main function to run """
    log.info('Started')
    with InitNornir(config_file=NR_CONFIG) as nr:
        # with InitNornir(config_file=NR_CONFIG, inventory="inventory.inv.XlsInventory") as nr:
        # print(nr.inventory.hosts)
        # print(nr.inventory.hosts['test-host1'].data)
        # for command in TEMP_COMMANDS:
        for command in SDWAN_CONFIGS:
            run_send_command(nr, command, role='cedge')
            # run_send_command(nr, command, group='nidcsdwan-cedge')
            # run_send_command(nr, command, name='r12.csdwan.lab')
            pass
        for command in ONLY_CONFIG:
            run_send_command(nr, command, role='intermediate')
            # run_send_command(nr, command, group='nidcsdwan-intermediate')
            # run_send_command(nr, command, name='HQ-ASW-4A-1')
            pass

    if FAILED:
        print(f"\n{'~' * 20}")
        print(f"- Unable to play with following hosts:\n {', '.join(FAILED)}")
    else:
        print('+ All done')


# MAIN
if __name__ == "__main__":
    main()
